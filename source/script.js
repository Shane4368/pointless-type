/* -----------------------------------------------------------------------------
	Copyright (c) 2019-2022 Shane Harris. All rights reserved.
----------------------------------------------------------------------------- */



/**
 * @type {HTMLInputElement}
 */
const keyboardInput = document.getElementById("keyboard");

keyboardInput.addEventListener("keydown", (/**@type {KeyboardEvent} */ e) =>
{
	if (e.key === "Enter")
	{
		keyboardInput.value = keyboardInput.value.trim();
		const codeScreen = document.getElementById("screen");

		if (keyboardInput.value === "/clear")
		{
			codeScreen.textContent = null;
		}
		else
		{
			codeScreen.textContent += `${keyboardInput.value}\n`;
			const parent = codeScreen.parentElement;
			parent.scrollTop = parent.scrollHeight;
		}

		keyboardInput.value = "";
	}
});



/**
 * @type {HTMLButtonElement}
 */
const themeToggleButton = document.getElementById("theme-toggle-button");

themeToggleButton.addEventListener("click", () =>
{
	const currentTheme = document.documentElement.dataset.theme;

	if (currentTheme === "light")
	{
		setTheme("dark");
		themeToggleButton.dataset.theme = "🌑";
	}
	else
	{
		setTheme("light");
		themeToggleButton.dataset.theme = "☀️";
	}
});

// ensure button text matches current theme
themeToggleButton.click();
themeToggleButton.click();