/**
 * @param {string} theme
 */
function setTheme(theme)
{
	document.documentElement.dataset.theme = theme;
	window.localStorage.setItem("theme", theme);
}

setTheme(window.localStorage.getItem("theme") ?? "light");